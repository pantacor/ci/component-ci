#!/bin/bash

set -e

while getopts "fh" o; do
	case $o in
		f)
			FINAL="true"
			;;
		h)
			echo "Usage:"
			echo "mkdocker.sh [options]"
			echo ""
			echo "	-f	generate final dockerfile"
			echo "	-h	help (this output)"
			exit 0
			;;
	esac
done

JOBS=${JOBS:-amd64:x86_64:musl: amd64:x86_64:glibc: arm32v6:arm:musl:arm-linux-musleabihf arm32v5:arm:glibc:arm-linux-gnueabi}

if [ -f component-ci.conf ]; then
	source component-ci.conf;
else
    declare -p JOBS | cut -d " " -f 3- > component-ci.conf;
	source component-ci.conf;
    cp component-ci/Dockerfile.template.* .
    cp ./component-ci/Dockerfile.template.* .
fi

cat > .gitlab-ci.yml << EOF
include:
  remote: https://gitlab.com/pantacor/ci/ci-templates/raw/master/component-ci.yml

EOF

for j in $JOBS; do
	arch=`echo $j | sed -e 's/:.*//'`
	qemu_arch=`echo $j | sed -e 's/.*:\(.*\):.*:.*/\1/'`
	libc=`echo $j | sed -e 's/.*:.*:\(.*\):.*/\1/'`
	cross=`echo $j | sed -e 's/.*://'`

	dockerfile_name="Dockerfile.template.${libc}"
    if [ "$cross" != "" ]; then dockerfile_name="${dockerfile_name}.cross"; fi

	if [ -n "$FINAL" ]; then cat $dockerfile_name | ARCH=$arch QEMU_ARCH=$qemu_arch LIBC=$libc CROSS=$cross envsubst > Dockerfile.$arch.$libc; fi
	cat >> .gitlab-ci.yml << EOF2

build-$arch-$libc:
  extends: .build-component
  variables:
    ARCH: "$arch"
    QEMU_ARCH: "$qemu_arch"
    LIBC: "$libc"
    CROSS: "$cross"
EOF2
done
