FROM debian:stretch as qemu

RUN apt-get update && apt-get install -y wget && apt-get clean && rm -rf /var/lib/apt/lists

RUN wget -O /qemu-${QEMU_ARCH}-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-${QEMU_ARCH}-static; \
	chmod a+x /qemu-${QEMU_ARCH}-static

FROM ${ARCH}/debian:stretch as sysroot

COPY --from=qemu /qemu-${QEMU_ARCH}-static /usr/bin/

# INSTALL BUILD DEPENDENCIES
RUN apt-get update && apt-get install -y \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists

FROM registry.gitlab.com/pantacor/platform-tools/docker-${LIBC}-cross-${QEMU_ARCH}:master as crossbuilder
COPY --from=sysroot / /sysroot-${QEMU_ARCH}

WORKDIR /work

# COPY SOURCE CODE
COPY . src

# INSTALL MAKE DEPENDENCIES
RUN apt-get update && apt-get install -y \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists

# CROSSBUILD
RUN mkdir out

FROM ${ARCH}/debian:stretch

COPY --from=qemu /qemu-${QEMU_ARCH}-static /usr/bin/

# INSTALL RUNTIME DEPENDENCIES
RUN apt-get update && apt-get install -y \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists

# COPY BUILD ARTIFACTS
COPY --from=crossbuilder /work/out /usr/local

CMD [ "/lib/systemd/systemd" ]
