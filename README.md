# COMPONENT CI TEMPLATE

## Introduction

This template aims to avoid repetitive work when creating a new component CI. This CI will be in charge of compiling source code to maintain a registry of installable binaries.

## How to use the CI template

To use it, just add this template to your project as a submodule:

```
git submodule add https://gitlab.com/pantacor/ci/component-ci.git
```

After that, you will need to execute our mkdocker.sh script from your newly added submodule:

```
./component-ci/mkdocker.sh
```

This will create a new component-ci.conf file, the .gitlab-ci.yml file and a series of dockerfile templates. You can now modify component-ci.conf and the dockerfile templates to fit your project needs. After modifying that, remember to execute mkdocker.sh again.

### component-ci.conf

This configuration file will be generated for the first time if it does not exist in your project. It will define a list of jobs that will look like this:

```
JOBS="amd64:x86_64:musl: amd64:x86_64:glibc: arm32v6:arm:musl:arm-linux-musleabihf arm32v5:arm:glibc:arm-linux-gnueabi"
```

Each job is defined following this syntax:

```
<architecture>:<qemu-architecture>:<libc>:[cross-compiler]
```

The template used for each job will be defined by the last two arguments. For example, for the job:

```
amd64:x86_64:musl:
```

The used dockerfile template will be Dockerfile.template.musl, as the cross-compiler argument has not been set.

**IMPORTANT:** Although this config file is not directly used by the GitLab CI, you will need to commit it if you want your jobs definition to persist in future uses of mkdocker.sh.

### dockerfile templates

The dockerfile template are the definition of the output component containers. There are four templates, and you will need to modify them acording to the jobs defined in your component-ci.conf:

* Dockerfile.template.glibc: native compilation of glibc binaries
* Dockerfile.template.musl: native compilation of musl binaries
* Dockerfile.template.glibc.cross: cross compilation of glibc binaries
* Dockerfile.template.musl.cross: cross compilation of musl binaries

In these templates, you will need to set the build dependencies, build process specific to your project and runtime dependencies.

To check how the final dockerfiles will look like, use the -f option in mkdocker.sh.

**IMPORTANT:** You will need to commit the template or templates used by your project. It is not mandatory to commit the non used templates or the final templates generated with mkdocker-sh -f.

### .gitlab-ci.yml

This GitLab CI definition will contain the jobs generated with mkdocker.sh using component-ci.conf. Check how this file looks before pushing it to GitLab, and if it triggers a new CI pipeline after that. Every new commit (For example: making changes to your source code) should now trigger a CI pipeline, so you will be able nothing is broken after a new patch in your project.

**IMPORTANT:** This file must be commited to your project.

## How to use the binaries from other projects

After success, CI will have created a new entry in the GitLab registry. With this you can do the following:

### Test it locally

To test it in your host computer, just run it using docker:

```
docker run -it --rm registry.gitlab.com/pantacor/c-green1/lime-suite:amd64
```

*NOTE:* registry URL may vary depending on your project

### Install it in a dockerfile

To include the binaries in your dockefile:

```
FROM registry.gitlab.com/pantacor/c-green1/lime-suite:${ARCH}-glibc-19-01-container-ci as lime-suite

COPY --chown=0:0 --from=lime-suite /usr/local/ /usr/local/
```

*NOTE:* location may vary depending on your project
